TO clear datastore:
    1. up docker container:
        host$ docker-compose up -d node
    2. rm data store file:
        host$ docker-compose exec node rm /root/.config/gcloud/emulators/datastore/WEB-INF/appengine-generated/local_db.bin
    3. restart container:
        host$ docker-compose restart node
